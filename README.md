## Trivore ID Client SDK

The Client SDK that wraps around the TrivoreID APIs.

This Client SDK covers only the most basic API requests. The rest endpoints are
covered in the [extended version](https://gitlab.com/trivore_public/trivoreid-client-sdk-for-python-extended).

Documentation to get started and use Services can be found [here](https://trivore.atlassian.net/wiki/spaces/TISpubdoc/pages/20515307/Client+SDK+for+Python).

### PyPi Package

History of all releases can be found [here](https://pypi.org/project/trivoreid/).

Installing with pip:

```
pip install trivoreid
```